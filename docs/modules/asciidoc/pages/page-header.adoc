= Page Header
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:linkattrs:
:note-caption: Ops Hint
// URLs
:uri-adoc-manual: https://asciidoctor.org/docs/user-manual
:uri-author: {uri-adoc-manual}/#author-and-email
:uri-attrs: {uri-adoc-manual}/#attributes
:uri-builtin-attrs: {uri-adoc-manual}/#builtin-attributes

On this page, you'll learn:

* [x] How to structure a valid AsciiDoc page header.
* [x] How to set a page title.
* [x] How to add metadata to a page.
* [x] How to set AsciiDoc's built-in page attributes.

== Header anatomy

The page header is a set of contiguous lines that start on the first line of the file.
The header is separated from the page body by at least one blank line.

.Common elements in a page header
[source,asciidoc]
----
= Page Title // <1>
First Middle Last <author@email.com> // <2>
:description: A description of the page stored in an HTML meta tag. // <3>
:keywords: comma-separated values, stored, in an HTML, meta, tag // <4>
:sectanchors: // <5>
:url-repo: https://my-git-repo.com // <6>
----
<1> Title of the page
<2> Author name and email address
<3> Description attribute
<4> Keywords attribute
<5> Example of a built-in page attribute
<6> Example of a user created and defined page attribute

Each attribute entry must be entered on its own line.
Attributes can be placed in any order in the header, including above the page title.
However, the preferred placement is below the title.
The header can also contain xref:comments.adoc[comments].

[#page-title]
== Page title

A page title is specified by one equals sign (`=`), followed by one blank space, and then the text of the title.

.Page title syntax
[source,asciidoc]
----
= The title of this page
----

[#page-meta]
== Page metadata

AsciiDoc provides several optional, built-in attributes for defining page metadata.

=== Author syntax

Specifying the author or authors of a page is optional.
The author is listed on the line directly beneath the page’s title.
An optional email address or contact URL can follow an author’s name inside a set of angle brackets (`< >`).
When a page has multiple authors, each author is separated by a semicolon (`;`).

.Multiple authors and author information syntax
[source,asciidoc]
----
= Page Title
First Middle Last <author@email.com>; First Last <author@email.com>
----

Author names are output to the HTML `<meta>` tag.

.Author HTML output
[source,html]
....
<head>
<meta charset="UTF-8">
<meta name="author" content="First Middle Last, First Last">
....

Whether any author information is also displayed on a published page depends on the site's UI templates.

[discrete]
==== Asciidoctor resources

Refer to the Asciidoctor user manual for additional author attributes and methods for specifying author information.

* {uri-author}[Author attributes and usage examples^]

=== Description syntax

If set, `description` is output to an HTML `<meta>` tag with the same name.
You can break long values across several lines by ending each line with a backslash `\` that is preceded by a space.

.Description attribute syntax
[source,asciidoc]
----
= Page Title
:description: A description of the page stored in an HTML meta tag. This page is \
about all kinds of interesting things.
----

.Description HTML output
[source,html]
....
<head>
<meta charset="UTF-8">
<meta name="description" content="A description of the page stored in an HTML meta tag. This page is about all kinds of interesting things.">
....

=== Keywords syntax

The keywords attribute contains a list of comma-separated values that are assigned to an HTML `<meta>` tag with the same name.

.Keywords attribute syntax
[source,asciidoc]
----
= Page Title
:keywords: comma-separated values, stored, in an HTML, meta, tag
----

.Keywords HTML output
[source,html]
....
<head>
<meta charset="UTF-8">
<meta name="keywords" content="comma-separated values, stored, in an HTML, meta, tag">
....

[#page-attrs]
== Built-in page attributes

AsciiDoc provides numerous built-in attributes that can activate and control syntax output behavior and styles.

TIP: If you're not familiar with AsciiDoc attribute restrictions or operations precedence, review the {uri-attrs}[attributes section of the Asciidoctor manual^].

Page attributes are set in the header and are available to the whole page.
Built-in AsciiDoc attributes are reserved attributes that have special, pre-defined behavior.
Many built-in attributes also have a restricted set of accepted values.

Built-in page attributes usually do two things; they toggle a behavior on or off (boolean), and they change the generated output by accepting an alternate value or replacement content (variable).

=== Set and unset built-in attributes

Let's turn on the attribute `sectanchors`.

.Set a built-in page attribute
[source,asciidoc]
----
= Page Title
:sectanchors:
----

When turned _on_, `sectanchors` adds an anchor to the left of each xref:section-headings.adoc[section title].
The anchor is rendered as the symbol `§`.
The attribute is turned on, also known as _set_, by simply entering it into the header.

Built-in attributes that are on by default, like `table-captions`, can be _unset_ (turned _off_) with a leading or trailing `!` added to its name.

.Unset a built-in page attribute
[source,asciidoc]
----
= Page Title
:sectanchors:
:table-caption!:
----

=== Change a built-in attribute value

Let's look at an example of a built-in attribute that has a default value that we want to replace with a custom value.

The label on a xref:admonitions.adoc[Note admonition] is controlled by the attribute `note-caption`.
This attribute is set (on) by default and has an implicit value of `NOTE`.
Let's change the value to "`OPS HINT`".

.Change a built-in page attribute value
[source,asciidoc]
----
= Page Title
:note-caption: OPS HINT
----

Now, when we create a Note admonition, its label is displayed as OPS HINT.

NOTE: This is an Ops Hint.

[discrete]
==== Asciidoctor resources

* {uri-builtin-attrs}[Built-in page attributes^]
* {uri-attrs}[AsciiDoc attribute restrictions or operations precedence^]
