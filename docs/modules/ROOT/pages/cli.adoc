= Command Line Interface
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:table-caption!:

Antora provides a command line interface (CLI) so you can interact with Antora using your terminal.
When installed, the CLI adds the `antora` command to your terminal's PATH.

== Terminal conventions

If you're new to using a terminal, these are the conventions we use when showing terminal examples.

Prompt (`$`)::
The terminal's command prompt is indicated by a dollar sign (`$`).
Don't include the prompt when you type commands.
Only type what immediately follows it.

Working Directory::
Every command is run from a directory, known as the working directory.
You may be asked to change the working directory of your terminal before running a command.
You can change your working directory using the `cd` command (e.g., `cd directory`).

Command Output::
If Antora returns information after a command is executed, the output is displayed in the terminal underneath the command.
The command prompt (`$`) is not displayed in the command output.

== Command structure

The `antora` command accepts user inputs in the form of options, positional arguments, and environment variables.

 $ antora <command> [options] <arguments>

. All functions start with a base call to Antora (`antora`).
. The command tells Antora what operation to perform.
. Additional options can be specified after a command.
. Positional arguments, such as the name of the playbook file, are specified last.
. Environment variables are read from the terminal's state.

== Commands and options

.CLI Commands
[cols="1,2" width=65%]
|===
|Command |Purpose

|antora
|Required base command.

|generate [options] <playbook>
|Generate the documentation site specified by the <playbook>.
This command is implicitly executed if no command is specified.
<<cli-run-ex,See examples>>.
|===

When executed from the CLI, the options that are mapped to playbook keys, such as `title`, will override any assigned values in the designated playbook.

[#cli-options]
.CLI Options
[cols="2,3,1,1"]
|===
|Option |Purpose |Format |Default

|--cache-dir <dir>
|Directory where cached files are stored (git repositories clones and the UI bundle).
Overrides the xref:playbook:playbook-schema.adoc#cache-dir-key[cache dir value] specified in the playbook.
|String
|[.path]_<user's cache>/antora_

|--clean
|Remove the xref:playbook:playbook-schema.adoc#clean-key[output directory] before publishing the site.
|Boolean
|false

|--pull
|Download updates to remote resources (content sources and UI bundle).
|Boolean
|false

|--quiet
|Do not write any messages to stdout.
|Boolean
|false

|--silent
|Suppress all messages, including warnings and errors.
|Boolean
|false

|--stacktrace
|Print the stacktrace to the console if the application fails.
|Boolean
|false

|--title <title>
|Specify the title of the site.
Overrides the xref:playbook:playbook-schema.adoc#site-title-key[site title value] specified in the playbook.
|String
|not set

|--to-dir <dir>
|Directory where the site is generated.
Overrides the xref:playbook:playbook-schema.adoc#dir-key[output dir value] specified in the playbook.
|String
|[.path]_build/site_

|--ui-bundle <bundle>
|UI bundle URL.
The URL can be a path on the local filesystem.
Overrides the xref:playbook:playbook-schema.adoc#ui-bundle-key[ui bundle value] specified in the playbook.
|String
|not set

|--url <url>
|Base URL of the published site.
The URL should not include a trailing slash.
Overrides the xref:playbook:playbook-schema.adoc#site-url-key[site url value] specified in the playbook.
|String
|not set

|-v, --version
|Output the Antora version information.
|Built-in
|n/a

|-h, --help
|Output the command usage information.
|Built-in
|n/a
|===

== Get help with the CLI

When you're using the Antora CLI and need help, type `-h` or `--help` after the command.

.Display help for the antora command
 $ antora --help

.Display help for the generate command
 $ antora generate -h

[#cli-run-ex]
== Run the generate command

You can run the generate command implicitly or explicitly.

.Example 1: Run the generate command (implicit)
 $ antora site.yml

In Example 1, Antora generates a documentation site using the playbook [.path]_site.yml_.

.Example 2: Run the generate command (explicit)
 $ antora generate test-site

In Example 2, Antora generates a documentation site using the auto-detected playbook [.path]_test-site.yml_.
When the playbook argument doesn't have a file extension, Antora will look for a YAML, JSON, or CSON file matching the playbook name (in that order).

.Example 3: Run the generate command with --to-dir option (implicit)
 $ antora --to-dir prod site.yml

In Example 3, Antora generates a documentation site using the playbook [.path]_site.yml_.
A directory named [.path]_prod_ will be created (relative to the current working directory) and the site files written to it.

.Example 4: Run the generate command with --to-dir and --title options (explicit)
 $ antora --to-dir site --title "My Awesome Docs" beta-site.json

In Example 4, Antora generates a documentation site using the playbook [.path]_beta-site.json_.
The site title will be _My Awesome Docs_.
A directory named [.path]_site_ will be created (relative to the current working directory) and the site files written to it.

.Example 5: Have the generate command download updates
 $ antora --pull site.yml

After running the generate command the first time, subsequent runs will use cached copies of remote resources by default (effectively running offline).
Example 5 shows how to run the generate command so it will download (fetch) updates to remote content sources and download a remote UI bundle again.
