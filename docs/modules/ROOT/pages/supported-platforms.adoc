= Supported Platforms
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
:linkattrs:
// URIs
:uri-repo: https://gitlab.com/antora/antora
:uri-issues: {uri-repo}/issues
:uri-chat-dev: https://gitter.im/antora/dev

Antora operates on Linux, macOS, and Windows and should operate as designed on common VM environments and cloud platforms.

== Tested platforms

Antora is comprehensively tested on these platform.

// When we have specific notes about a platform (tweaks and/or links to bugs) add a 3rd column to this table labeled "Good to Know"
[cols="20,25a",width="45%"]
|===
|Supported Platform |Supported Version(s)

|Alpine Linux
|[%hardbreaks]
3.6
3.7

|Debian Linux
|9.3

|Fedora Linux
|[%hardbreaks]
26
27

|macOS
|High Sierra

|Microsoft
|[%hardbreaks]
Windows 8.1
Windows 10
Windows Server 2012 R2
Windows Server 2016

|Chrome
|Latest stable version

|Firefox
|Latest stable version

|Internet Explorer
|11

|MS Edge
|Latest stable version

|Node
|8.10
|===

== Hardware recommendations

Your hardware requirements will depend primarily on the size of the remote Git repositories your Antora pipeline fetches content from in order to generate site.

The first time Antora runs, it fetches and clones all of the remote Git repositories specified in a playbook.
The speed of this operation is dictated by the size of the remote repositories, environment input/output parameters, and network performance.
After the initial run, Antora caches the repositories locally.
On subsequent runs, Antora will only reconnect to the remote repositories if the xref:cli.adoc#cli-options[`pull` key is on] or if the xref:playbook:configure-runtime.adoc[local cache is removed].
// Add xref to pull key when documentation is available

[cols="5s,70",width="75%"]
|===
|RAM
|Memory requirements depend on the size of your Git repositories.
3GB should provide performance headroom.

|I/O
|Maximum throughput and minimum latency always make things nicer but aren't required.
|===

== Cloud platforms

Antora should operate as designed on common cloud platforms.
How you provision your cloud instance depends on your workload requirements and remote Git repository sizes.
If you're running Antora on a cloud platform, we would love to hear about your experience (open a {uri-issues}[new issue^] or join us in the {uri-chat-dev}[development channel^]).

== Virtual Machine (VM) environments

Antora runs in VirtualBox and should operate as designed in common VM environments such as VMware.
If you're running Antora in a VM environment, we would love to hear about your experience (open a {uri-issues}[new issue^] or join us in the {uri-chat-dev}[development channel^]).

////
OS Release schedule links

Alpine: https://wiki.alpinelinux.org/wiki/Alpine_Linux:Releases
- 3.7 ends 2019-11-01
- 3.6.2 ends 2019-05-01, gets security fixes only

Arch: https://www.archlinux.org/releng/releases/

Arch Linux releases once a month, with only the 3 most recent distros being officially available

Debian: https://www.debian.org/releases/stable/

Fedora: https://fedoraproject.org/wiki/Releases

Fedora 28 will be released May 2018
Fedora 26 will be maintained until 1 month after the release of Fedora 28.

Ubuntu Linux: https://wiki.ubuntu.com/Releases

16.04.3 LTS is the most recent LTS release.
17.10 is the most recent regular release, EOL July 2018
18.04 LTS is due to be released in April 2018

openSUSE: https://en.opensuse.org/Portal:42.3

Leap 42.3 is the current release
Checkout the Open Build Project: http://openbuildservice.org

Windows: https://en.wikipedia.org/wiki/Comparison_of_Microsoft_Windows_versions

Windows 8.1 is supported by MS until 2023-01-10
Windows Server 2012 R2 until 2023-10-10
Windows Server 2016 / Windows 10 don't have an EOL date (as of 2018-03-10)

Antora is tested on NTFS on Windows, we may want to checkout how it works on the ReFS on Windows Server 2016 in the future

MS Edge: replaces IE 11 which is not being developed further; it is the default browser for Windows 10/Server 2016
////
