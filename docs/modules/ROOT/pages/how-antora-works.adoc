= Pipeline Process
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// Settings
:idprefix:
:idseparator: -
//The playbook is a configuration file that contains an inventory of documentation component names, branches, and addresses.

The steps listed on this page describe the operations the default site generator pipeline performs.
Keep in mind that Antora is a modular pipeline.
This architecture allows special use case and custom components, such as validators, to be inserted into the pipeline at multiple points as needed.
Also, the pipeline executes operations asynchronously, so many of the steps listed below may occur in a different order or simultaneously.
For example, the UI is loaded as early as possible, then waits until its templates are needed.

== Default Pipeline Steps

Read playbook::
When you're ready to generate a site, you give Antora a playbook and tell it to run.
The pipeline loads and reads the playbook.
+
A xref:playbook:playbook.adoc[playbook is a configuration file], written in YAML, JSON, or CSON, that tells Antora what content to use, how the content should be processed, how the site should be generated, and where to publish the output.

Locate content repositories and branches::
Using the xref:playbook:configure-content-sources.adoc[content source URLs and branch names] listed in the playbook, Antora searches for and locates the specified repositories and branches.

Clone or fetch remote content repository branches::
Antora downloads the contents of each remote repository and branch it located.

Find the documentation component in each branch::
Antora identifies a group of files as a documentation component when it finds a component descriptor file named [.path]_antora.yml_.
+
A xref:component-structure.adoc[documentation component] is a set of files that are versioned together and  usually share a common subject, such as all of the content source files for a software product.
Component source files can be stored in a single repository, at the root of a repository or in a subpath, or distributed across multiple repositories.

Transform input files into virtual file objects::
Antora collects all of the files--text, images, samples, and other supporting materials--from each documentation component subtree.
Then it creates a virtual file of each input file.

Assign files to component-version collections::
Antora reads the component descriptor (i.e., [.path]_antora.yml_) files.
+
A xref:component-descriptor.adoc[component descriptor] associates the files under it with a specified component name and version (aka component-version).
This allows the files to be decoupled from the repository and branch in which they live.
The component descriptor file makes Antora's repository-spanning, file system and URL agnostic xref:asciidoc:page-to-page-xref.adoc[cross references] possible.
+
Antora assigns the descriptor information and other source metadata to the appropriate virtual files.
Then it sorts each file into a virtual collection based on the assigned component-version data.

Compute additional metadata::
Using xref:playbook:configure-site.adoc[site properties] from the playbook and information assigned to the files in the previous step, Antora adds module, family, family-relative path, and other metadata values to each file.
It also computes the output path (disk) and publish path (URL) information for each publishable file.

Organize files into a content catalog::
Antora further sorts the aggregated files into a content catalog that can be queried and transmitted.

// add pages xref when page is available
Convert AsciiDoc files to embeddable HTML::
The AsciiDoc files in the xref:pages.adoc[page family] of the content catalog are converted to embeddable HTML with Asciidoctor.js.

Convert navigation files::
Antora retrieves xref:navigation:index.adoc[navigation files] from the content catalog, translates their contents into navigation items organized in a specified hierarchy (navigation trees grouped inside a navigation menu), and returns a navigation model.

Locate and fetch UI bundle::
Antora finds the xref:playbook:configure-ui.adoc[UI bundle using the URL listed in the playbook] and fetches it.
The UI bundle can be cached locally or remote.

Transform UI files into virtual file objects::
Antora extracts the UI files in the bundle and creates a virtual file object for each file containing the file's contents and path information.

Classify UI files::
Antora identifies the static UI files using the UI descriptor file ([.path]_ui.yml_) and sets the file type to static.
It sets the type for all other files based on their location (asset, layout, helper, partial).

Compute UI file output paths::
For each UI file that is publishable (of type static or asset), Antora computes its output path.

Organize UI files into UI catalog::
The virtual UI files are sorted into a transmittable collection.

Wrap converted AsciiDoc content in page templates::
Antora determines which page template each page requests.
It populates the identified UI template with the page's embeddable HTML, site metadata, context data (component, version) for page, version, and product selectors, and navigation model information for menus and breadcrumbs.
+
Relying on a page template to produce the pages gives the site owner complete control over the construction of the pages, and thus complete control over the UI.

Produce sitemap::
Antora generates a site map that can be used as an internal report or published with the site.

Publish site::
Antora writes the generated pages to a default or xref:playbook:configure-output.adoc#output-dir[user-specified location].
The site can be published in multiple formats to multiple locations over multiple protocols using xref:playbook:configure-output.adoc#output-provider[built-in and custom destination providers].
